import {Component} from '@angular/core';
/* 0 - IMPORTAR MOVIDAS */
import {ConnectBDService} from "../../shared/services/connect-bd.service";
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';



@Component({
  selector: 'app-ranking',
  templateUrl: './ranking.component.html',
  styleUrls: ['./ranking.component.css']
})


export class RankingComponent {

  /* 1 - CONSTRUCTOR */
  constructor(private connectbd: ConnectBDService) {
  }

  /* 2 - DATOS QUE MANEJAMOS */
  data: any;

  /* 3 - FUNCIÓN QUE LLAMA A LA API */
  getRanking() {
    this.connectbd.getClassificacio().subscribe(res => {
      console.log(res);
      if (res.data.length == 0) {
        this.data = [];
      } else {
        this.data = res.data;
      }
    });
  }

  /* 4 - DESCARGAR PDF */
  downloadPDF()  {
    const DATA: HTMLDivElement = document.getElementById('htmlData') as HTMLDivElement;
    const doc = new jsPDF('p', 'pt', 'a4');
    //Orientation: portrait //Unit: points (también se podría poner 'mm', 'cm', 'm', 'in' o 'px' // Formato: a4
    const options = {
      background: 'white',
      scale: 3
    };
    html2canvas(DATA, options).then((canvas) => {
      const img = canvas.toDataURL('image/PNG');
      // Add image Canvas to PDF
      const bufferX = 15;
      const bufferY = 15;
      const imgProps = (doc as any).getImageProperties(img);
      const pdfWidth = doc.internal.pageSize.getWidth() - 2 *
        bufferX;
      const pdfHeight = (imgProps.height * pdfWidth) /
        imgProps.width;
      doc.addImage(img, 'PNG', bufferX, bufferY, pdfWidth,
        pdfHeight, undefined, 'FAST');
      return doc;
    }).then((docResult) => {
      docResult.save(`${new Date().toISOString()}.pdf`);
    });
  }

}
