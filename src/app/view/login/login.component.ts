import { Component } from '@angular/core';
import {ConnectBDService} from "../../shared/services/connect-bd.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {User} from "../../shared/classes/User";
import {nomValidator} from "./validar-password.directive";
import { emailValidator } from "./validar-password.directive";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent {

  constructor(private connectdb: ConnectBDService) {   }

  loginForm!: FormGroup;
  users : User[] = [];
  message!: string;
  OK!: boolean;

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      nom : new FormControl('',
        [Validators.required, Validators.minLength(3),
          Validators.email, emailValidator()]),
      password: new FormControl('',
        [Validators.required,
          Validators.minLength(8), nomValidator() ])
    })
  }

  get nom() {
    return this.loginForm.get('nom');
  }

  get password() {
    return this.loginForm.get('password');
  }

  postLogin() {
    this.connectdb.login(this.loginForm.value).subscribe(res => {
      if (res.data.length == 0) {
        this.users = [];
        this.message = "Login incorrecte. ";
        this.OK = false;
      } else {
        this.users = res.data;
        this.message = "L'usuari " + this.users[0].nom
          + " s'ha loguejat de manera correcta!!!";
        this.OK = true;
      }
    })
  }

  /*onSubmit() {

    this.connectdb.login(this.loginForm.value).subscribe(res => {
      if (res.data.length == 0) {
        this.users = [];
        this.message = "Login incorrecte. ";
      } else {
        this.users = res.data;
        this.message = "L'usuari " + this.users[0].nom
          + " s'ha loguejat de manera correcta!!!";
      }
    })
  }*/
}
