import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from "@angular/router";

import { GameComponent } from "./view/game/game.component";
import { HelpComponent } from "./view/help/help.component";
import { RankingComponent } from "./view/ranking/ranking.component";
import { LoginComponent } from "./view/login/login.component";
import { RegisterComponent } from "./view/register/register.component";
import { HomeComponent } from "./view/home/home.component";

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'Juego', component: GameComponent },
  { path: 'Ayuda', component: HelpComponent },
  { path: 'Login', component: LoginComponent },
  { path: 'Registro', component: RegisterComponent },
  { path: 'Clasificacion', component: RankingComponent }
];

@NgModule({
  declarations: [],
  exports: [RouterModule],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ]
})
export class AppRoutingModule { }
