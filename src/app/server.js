/* pas 1: ja fet: crearem un projecte d’Angular en comptes de Node.js. */

/* pas 2: Importar la llibreria Express i inicialitzar una variable amb express. */
var express = require('express');
var app = express();

/* pas 3: Importar les llibreries de mysql, cors i body-parser. */
var mysql = require('mysql');
var cors = require('cors');
var bodyParser = require('body-parser');

/* pas 4: Indiquem que utilitzi cors per evitar problemes amb el navegador. */
// Configuración avanzada de CORS
const corsOptions = {
  origin: 'http://localhost:4200', // Permitir solo este origen
  methods: 'GET,HEAD,PUT,PATCH,POST,DELETE', // Métodos permitidos
  allowedHeaders: ['Content-Type', 'Authorization'] // Cabeceras permitidas
};

app.use(cors(corsOptions));

/* pas 5: Indicar que utilitzarem json i que rebrem peticions amb url. */
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

/* pas 6: Indiquem que acceptem peticions pel port 3001: */
// set port
app.listen(3001, function () {
  console.log('Node app is running on port 3001');
});
module.exports = app;

/* pas 7: Establim la connexió amb la BD */
var dbConn = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'super3',
  database: 'webanimes'
});

/* Pas 7 "extra": connectar a la BBDD */
dbConn.connect(function (err) {
  if (err) {
    console.error('Error connecting to the database: ' + err.stack);
    return;
  }
  console.log('Connected to the database.');
});

/* pas 8: cal executa el següent script només la primera vegada que fas Node.js amb Express al mysql:
    ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'super3';
*/

/* pas 9: Creem la petició per defecte */
app.get('/', function (req, res) {
  return res.send({ message: 'Hola tio' });
});

/* PACMAN */
app.get('/getClassificacio', function (req, res) {
  dbConn.query('SELECT * FROM classificacio ORDER BY punts desc', function (error, results, fields) {
    if (error) {
      console.error(error);
      return res.status(500).send({ error: true, message: 'Error retrieving data' });
    }
    return res.send({ error: false, data: results, message: "Ranking" });
  });
});

/* LOGIN PACMAN */
app.post('/login', function (req, res) {
  let nom = req.body.nom;
  let password = req.body.password;

  dbConn.query('SELECT * FROM users WHERE nom = ? AND password = ?', [nom, password], function (error, results, fields) {
    if (error) {
      console.error(error);
      return res.status(500).send({ error: true, message: 'Error during login' });
    }
    return res.send({ error: false, data: results, message: 'Login successful' });
  });
});

app.post('/registerUserPacman', function (req, res) {
  let id = req.body.id;
  let nom = req.body.nom;
  let password = req.body.password;

  dbConn.query('INSERT INTO users SET ?', { id: id, nom: nom, password: password }, function (error, results, fields) {
    if (error) {
      console.error(error);
      return res.status(500).send({ error: true, message: 'Error: ID REPETIDA' });
    }
    return res.send({ error: false, data: results, message: 'Registro realizado' });
  });
});
